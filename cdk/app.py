#!/usr/bin/env python3
import os
import aws_cdk as cdk
#from constructs  import Construct
#from cdkv2.cdkv2_stack import Cdkv2Stack

aws_region = os.environ.get("AWS_DEFAULT_REGION", "us-east-2")
aws_account = os.environ.get("AWS_ACCOUNT_ID", "492398003379")


app = cdk.App()
cdk.Stack (
    app, 
    "valheim-server-stack",
    env={"region": aws_region, "account": aws_account}
)

app.synth()
